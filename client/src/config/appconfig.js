import { env } from '../environment/env.js'

const services = {
    google: {
        clientid: env.DASHBOARD_GOOGLE_CLIENT_ID,
        clientSecret: env.DASHBOARD_GOOGLE_CLIENT_SECRET
    },
    facebook: {
        appId: env.DASHBOARD_FACEBOOK_APP_ID
    },
    spotify: {
        appId: env.SPOTIFY_KEY
    }
}

const api = {
    url: env.DASHBOARD_API_URL
}

export { services, api };