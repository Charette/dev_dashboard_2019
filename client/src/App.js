import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from "react-router-dom";
import Dashboard from "./pages/dashboard";
import Login from "./pages/login";
import Register from "./pages/register";
import MyDashboard from "./pages/myDashboard/index"
import NotFound from './pages/notFound'

const App = () => {
  return (
    <Router>
      <Switch>
        <Route exact path={"/"} component={Login} />
        <Route exact path={"/user"} component={Dashboard} />
        <Route exact path={"/register"} component={Register} />
        <Route exact path={"/dashboard"} component={MyDashboard} />
        <Route component={NotFound} />
      </Switch>
    </Router>
  )
}

export default App;
