import React from 'react';
import ReactHtmlParser from 'react-html-parser';

class Wikipedia extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            wikiSearchTerms: '',
            res: false
        }
    }

    changeWikiSearchTerms = (e) => {
        console.log(e.target.value)
        this.setState({
            WikiSearchTerms: e.target.value
        });
    }

    search = (e) => {
        e.preventDefault();
        // console.log(e.value)
        const that = this;
        fetch("https://en.wikipedia.org/api/rest_v1/page/summary/" + this.state.WikiSearchTerms)
            .then(function (response) {
                return response.json();
            })
            .then(function (jsonData) {
                that.setState({ apiInfo: jsonData });
                return JSON.stringify(jsonData);
            })
            .then(function (jsonStr) {
                // that.setState({ apiInfo: jsonStr });
                // console.log(jsonStr);
            });
    }

    render() {
        const api = this.state.apiInfo;
        
        console.log("api: ", this.state.apiInfo && this.state.apiInfo.title)
        return (
            <div className="App" style={{ maxWidth: "470px", marginLeft: "11px" }}>
                <h4 style={{ width: "100%", textAlign: "center", marginBottom: "10px"}}>Wikipedia</h4>
                <form action="" style={{maxWidth: "100%", textAlign: "center"}}>
                    <input style={{ marginRight: "5px" }} type="text" value={this.state.WikiSearchTerms || ''} onChange={this.changeWikiSearchTerms} placeholder='Search Wikipedia' />
                    <button onClick={this.search}>Search</button>
                </form>
                <div>
                    {api ?
                        [
                            <>
                                {/* <p> {api} </p> */}
                                <img src={api.thumbnail.source} style={{maxWidth: "100%", marginTop: "10px"}} />
                                <h4 style={{width: "100%", textAlign: "center"}}> {api.title} </h4>
                                <div style={{textAlign: "justify"}}> { ReactHtmlParser(api.extract_html) } </div>
                            </>
                        ]
                        : <p style={{width: "100%", textAlign: "center", marginTop: "10px"}}>No article selected</p>
                    }

                </div>
            </div> 
        );
    }
}

export default Wikipedia;