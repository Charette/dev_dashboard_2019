import React, { useState, useEffect } from 'react';

import moment from 'moment';

const News = ({ data }) => {
    const [news, setNews] = useState([]);

    useEffect(() => {
        fetch(`https://newsapi.org/v2/everything?q=${data}&apiKey=c9319a917d3d4eac97abc7bcaf33701d`)
            .then(res => res.json())
            .then(res => setNews(res.articles || []));
    }, []);


    return (
        <>
            <div style={{ width: "500px" }}>
                <h2 style={{ width: "100%", textAlign: "center", marginBottom: "20px", fontStyle: "italic" }}>News about {data}</h2>
                {
                    news.map((item, i) => {
                        var inputDate = item.publishedAt;
                        var outputDate = moment(inputDate).format("dddd, MMM DD at HH:mm a");
                        return (
                            <>
                                <div style={{ width: "480px", marginBottom: "20px", marginLeft: "10px" }} key={i}>
                                    <a href={item.url} target="blank">{item.title} : </a>
                                    <p >{outputDate}</p>
                                    <p style={{ textAlign: "justify" }}>{item.content}</p>
                                </div>
                            </>
                        );
                    })
                }
            </div>
        </>
    );

}

export default News;