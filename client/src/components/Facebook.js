import React, { Component } from 'react';
import { Redirect } from "react-router-dom";
import FacebookLogin from 'react-facebook-login';
import { FaFacebookF } from 'react-icons/fa';
import { services } from '../config/appconfig';

export default class Facebook extends Component {
    state = {
        isLoggedIn: false,
        userID: '',
        name: '',
        email: '',
        picture: '',
        redirect: ""
    }

    responseFacebook = response => {
        console.log(response);
        console.log("1", this.state.redirect);
        this.setState({ redirect: "/user" });
        console.log("Redirection", this.state.redirect);
    }

    componentClicked = () => {
        console.log("clicked");
    }

    render() {
        let fbContent;

        if (this.state.redirect) {
            return <Redirect to={this.state.redirect} />
        }
        if (this.state.isLoggedIn) {
            fbContent = null;
        } else {
            fbContent = (
                <FacebookLogin
                    appId={services.facebook.appId}
                    autoLoad={false}
                    fields="name,email,picture"
                    onClick={this.componentClicked}
                    callback={this.responseFacebook}
                    icon={<FaFacebookF />}
                />
            )
        }
        return (
            <div>{fbContent}</div>
        )
    }

}