import React from 'react';

import 'bootstrap/dist/css/bootstrap.min.css';
import logo from "../image/logo.PNG";


const Header = ({ data }) => {

    // const [users, setUsers] = useState([]);
    // useEffect(() => {
    //     fetch("http://localhost:8080/users/" + data)
    //         .then(res => res.json())
    //         .then(res => setUsers(res));
    // }, []);

    // console.log("data: ", data)
    return (
        <>
            <div style={styles.topImage} >
                <div style={styles.imageContainer} >
                    <a href={`http://localhost:3000/user?id=${data}#_=_`} >
                        <img src={logo} style={styles.image} alt="epidash logo" />
                    </a>
                </div>
            </div>
        </>
    );
}

const styles = {
    topImage: {
        backgroundColor: "lightblue",
        height: "100px",
        // display: "flex",
        // flexDirection: 'row',
        // justifyContent: "space-between",
        width: "100%", 
    },
    imageContainer: {
        position: 'relative',
        // maxWidth: "100%",
        // alignItems: "center",
        marginLeft: "42%"
    },
    image: {
        width: '200px',
        height: "auto",
        paddingLeft: "20px",
        marginTop: "12px"
    },
    nom: {
        marginTop: "35px"
    },
    log: {
        display: "flex",
        flexDirection: "row",
        marginTop: "35px",
        marginRight: "15px"
    },
    text: {
        marginRight: "10px",
        marginTop: "5px",
        color: "red"
    },
    picture: {
        marginRight: "10px",
        marginLeft: "-20px",
        borderRadius: "20%",
        marginTop: "-10px"
    }
};

export default Header;