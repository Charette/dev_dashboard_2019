import React, { useState, useEffect } from 'react';
import { Link } from "react-router-dom";
import { Navbar, Nav } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import logo from "../image/logo.PNG";
import Button from '@material-ui/core/Button';
import ExitToAppOutlinedIcon from '@material-ui/icons/ExitToAppOutlined';

const Header = ({ data }) => {

    const [users, setUsers] = useState([]);
    useEffect(() => {
        fetch("http://localhost:8080/users/" + data)
            .then(res => res.json())
            .then(res => setUsers(res));
    }, []);

    // console.log("data: ", data)
    return (
        <>
            <div style={styles.topImage} >
                <div style={styles.imageContainer} >
                    <a href={`http://localhost:3000/user?id=${data}#_=_`} >
                        <img src={logo} style={styles.image} alt="epidash logo" />
                    </a>
                </div>
                <div style={styles.nom}>
                    {users.picture ? <img src={users.picture && users.picture} style={styles.picture} alt="image8" /> : null}
                    Welcome,&nbsp;
                    <span><b>{users.firstName} &nbsp;{users.lastName}</b>  </span>
                    &nbsp;!
                </div>
                <div style={styles.log}>
                    <p style={styles.text} >LOG OUT</p>
                    <a href='http://localhost:8080/logout'>
                        <ExitToAppOutlinedIcon color="secondary" fontSize="large" />
                    </a>
                </div>
            </div>
            <div style={styles.navbar}>
                <Navbar bg="light" >
                    <Nav className="mr-auto" style={{ width: "100%" }}>
                        <Button href={`http://localhost:3000/user?id=${data}#_=_`}> Home </Button>
                        <Button><Link to={"/dashboard?id=" + users.userID + "#_=_"} className={"nav-link"} >My Widgets</Link></Button>
                    </Nav>
                </Navbar>
            </div>

        </>
    );
}

const styles = {
    topImage: {
        backgroundColor: "lightblue",
        height: "100px",
        display: "flex",
        flexDirection: 'row',
        justifyContent: "space-between",
        width: "100%",
    },
    imageContainer: {
        position: 'relative',
        maxWidth: "200px"
    },
    image: {
        width: '200px',
        height: "auto",
        paddingLeft: "20px",
        marginTop: "12px"
    },
    nom: {
        marginTop: "35px"
    },
    log: {
        display: "flex",
        flexDirection: "row",
        marginTop: "35px",
        marginRight: "15px"
    },
    text: {
        marginRight: "10px",
        marginTop: "5px",
        color: "red"
    },
    picture: {
        marginRight: "10px",
        marginLeft: "-20px",
        borderRadius: "20%",
        marginTop: "-10px"
    }
};

export default Header;