import React, { useState, useEffect } from 'react';
import Header from '../../components/header'
import { makeStyles } from "@material-ui/core";
import Button from '@material-ui/core/Button';
import queryString from "query-string";
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import Wikipedia from '../../components/widgets/wikipedia2';
import Spotify from '../../components/widgets/spotify';
import WeatherWidget from '../../components/widgets/weather';
import GifPlayer from 'react-gif-player';
import video from '../../image/waiting.gif';
import News from '../../components/widgets/news';
import { ReactSortable } from "react-sortablejs";
import { NavLink } from 'react-router-dom';
import DragIndicatorIcon from '@material-ui/icons/DragIndicator';
import CloseIcon from '@material-ui/icons/Close';

const MyDashboard = ({ history: { location } }) => {
    const classes = useStyles();
    const [services, setServices] = useState([]);
    const [services1, setServices1] = useState([]);
    // const [users, setUsers] = useState([]);
    const { search } = location;
    let user = queryString.parse(search).id;



    useEffect(() => {
        fetch("http://localhost:8080/services/" + user)
            .then(res => res.json())
            .then(res => setServices(res));
    }, [services1]);



    function suppService(id) {
        // console.log("id: ", id, user)
        if (user !== null && user.length > 0 && user.charAt(user.length - 1) === '/') {
            user = user.substring(0, user.length - 1);
        }
        const requestOptions = {
            method: 'DELETE',
        };
        fetch(`http://localhost:8080/services/${user}/${id}`, requestOptions)
            .then(res => res.json())
            .then(res => setServices1(res));
    }

    return (
        <>
            <Header data={user} />
            <div>
                {/* <Button onClick={test} /> */}
                <h2 style={{ width: "100%", textAlign: "center" }}>My Dashboard</h2>
                {services.length ? (
                    <div>
                        <ReactSortable className={classes.rootRoot}
                            list={services}
                            setList={setServices}
                            direction='horizontal'
                            swapThreshold='0.5'
                        >
                            {services.map((service, i) => {
                                if (service.serviceID === 4) {
                                    return (
                                        <Card className={classes.root} variant="outlined" style={{ maxHeight: "400px" }} key={i}>
                                            <DragIndicatorIcon></DragIndicatorIcon>
                                            <CardActions className={classes.button} >
                                                <Button size="small" style={{ marginLeft: "83%" }} className={classes.button} key={service.serviceID} id={service.serviceID} onClick={() => suppService(service.serviceID)} variant="contained" color="secondary"><CloseIcon style={{ maxWidth: "50%" }} /></Button>
                                            </CardActions>
                                            <Wikipedia />
                                        </Card>
                                    );
                                } else if (service.serviceID === 2) {
                                    return (
                                        <Card className={classes.root} variant="outlined" key={i}>
                                            <DragIndicatorIcon></DragIndicatorIcon>
                                            <Spotify />
                                            <CardActions style={{ width: "100%" }}>
                                                <Button size="small" className={classes.button} key={service.serviceID} id={service.serviceID} onClick={() => suppService(service.serviceID)} variant="contained" color="secondary">Delete from my dashboard</Button>
                                            </CardActions>
                                        </Card>
                                    )
                                } else if (service.serviceID === 5) {
                                    return (
                                        <Card className={classes.weather} variant="outlined" key={i}>
                                            <DragIndicatorIcon></DragIndicatorIcon>
                                            <WeatherWidget setting1={service.setting1} />
                                            <CardActions style={{ width: "100%" }}>
                                                <Button size="small" className={classes.weatherButton} key={service.serviceID} id={service.serviceID} onClick={() => suppService(service.serviceID)} variant="contained" color="secondary">Delete from my dashboard</Button>
                                            </CardActions>
                                        </Card>
                                    )

                                } else if (service.serviceID === 3) {
                                    return (
                                        <Card className={classes.root} variant="outlined" key={i}>
                                            <DragIndicatorIcon></DragIndicatorIcon>
                                            <CardActions style={{ width: "100%" }}>
                                                <Button size="small" className={classes.button} key={service.serviceID} id={service.serviceID} onClick={() => suppService(service.serviceID)} variant="contained" color="secondary"><CloseIcon style={{ maxWidth: "50%" }} /></Button>
                                            </CardActions>
                                            <News data={service.setting1} />
                                        </Card>
                                    )
                                }

                            })}
                        </ReactSortable>
                    </div>
                ) : (
                        <>
                            <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>

                                <h3 style={{ width: "100%", textAlign: "center", marginTop: "60px", color: "green" }}> No subbed Widgets, please select at least one in the front page </h3>
                                <GifPlayer gif={video} style={{ width: "400px", margin: "0 auto", marginTop: '30px', display: 'flex', justifyContent: 'center' }} />
                                <NavLink to={`/user?id=${user}`} className={classes.link}> Go to services page</NavLink>
                            </div>
                        </>
                    )
                }
            </div>
        </>
    )
}

const useStyles = makeStyles({
    rootRoot: {
        width: "100%",
        display: "flex",
        flexDirection: "row",
        flexWrap: "wrap",
        justifyContent: "space-evenly",
    },
    root: {
        minWidth: "500px",
        maxHeight: "400px",
        border: "2px solid black",
        margin: '40px',
        overflow: "auto",
    },
    title: {
        fontSize: 14,
        textAlign: "center",
        width: "100%"
    },
    pos: {
        display: "block",
        maxWidth: "100px",
        maxHeight: "100px",
        width: "auto",
        height: "auto",
        marginLeft: "30%",
        marginBottom: "18px",
        marginTop: "10px"
    },
    button: {
        marginLeft: "24%"
    },
    weather: {
        width: '300px',
        border: "2px solid black",
        margin: '40px',
        overflow: "auto"
    },
    weatherButton: {
        margin: "0 auto"
    },
    link: {
        // textDecoration: 'none',
        color: 'black',
        textAlign: 'center',
        marginTop: '30px',
        fontWeight: 'bold'
    }
});

export default MyDashboard;