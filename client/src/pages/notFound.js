import React from 'react';
import Header from '../components/header404'
import GifPlayer from 'react-gif-player';
import lost from '../image/lost.gif'

class NotFound extends React.Component {

    test = () => {
        this.props.history.goBack();
    }

    render() {
        return (
            <>
                <Header />
                <h2 style={{width: "100%", textAlign: "center", marginTop: "5%"}}>OOPS IT SEEMS YOU ARE LOST, CLICK ON THE GIF TO GO BACK</h2>
                <div style={{ width: "100%", height: "100%" }}>
                    <a onClick={this.test} ><GifPlayer gif={lost} style={{marginLeft: "33%", marginTop: "4%", minWidth: "600px"}} /></a>
                </div>
            </>
        )
    }
}

export default NotFound;