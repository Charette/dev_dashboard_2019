import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import React, { useState, useEffect } from 'react';
import TextField from '@material-ui/core/TextField';
import GifPlayer from 'react-gif-player';
import video from '../../image/congrats.gif';

// !widget ? ServicesPage : <div> linkto servicesPage +++ added services </div>

export default function Services({ data }) {
    const classes = useStyles();
    let WikiSearchTerms = "";
    let SearchTerms = "";
    const [services, setServices] = useState([]);
    const [services1, setServices1] = useState([]);

    useEffect(() => {
        fetch("http://localhost:8080/notServices/" + data)
            .then(res => res.json())
            .then(res => setServices(res));
    }, [services1]);

    function addService(id) {
        // console.log("id: ", data, "service: ", id)
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ userID: data, serviceID: id })
        };
        fetch(`http://localhost:8080/services/`, requestOptions)
            .then(res => res.json())
            .then(res => setServices1(res));
        // this.forceUpdate();
    }

    function addMeteo(id, city) {
        console.log("id: ", data, "service: ", id, "topic:", city)
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ userID: data, serviceID: id, setting1: city })
        };
        fetch(`http://localhost:8080/services/`, requestOptions)
            .then(res => res.json())
            .then(res => setServices1(res));
        // this.forceUpdate();
    }

    const changeWikiSearchTerms = (e) => {
        WikiSearchTerms = e.target.value;
        // console.log(e.target.value)
    }

    const changeSearchTerms = (e) => {
        SearchTerms = e.target.value;
        // console.log(e.target.value)
    }

    return (
        <>
            {services.length ?
                <>
                    <div className={classes.title} >
                        <h5>Choose a widget and add it to your dashboard: </h5>
                    </div>
                    <div className={classes.rootRoot} key={data}>
                        {services.map((service, i) => {
                            if (service.serviceID === 5) {
                                return (
                                    <Card className={classes.root} key={i} variant="outlined">
                                        <CardContent>
                                            <Typography variant="h5" component="h2" style={{ textAlign: "center" }}>
                                                {service.serviceName}
                                            </Typography>
                                            <Typography color="textSecondary">
                                                <img src={service.picture} className={classes.pos} alt="image4" />
                                            </Typography>
                                            <Typography variant="body2" component="p" style={{ textAlign: "justify" }} >
                                                {service.detail}
                                                <br />
                                            </Typography>
                                            <TextField id="outlined-basic" label="City" style={{ width: "100%" }} onChange={changeWikiSearchTerms} />
                                        </CardContent>
                                        <CardActions style={{ width: "100%" }}>
                                            <Button variant="contained" color="primary" size="small" className={classes.button} key={service.serviceID} id={service.serviceID} onClick={() => console.log("meteo", WikiSearchTerms) || addMeteo(service.serviceID, WikiSearchTerms)}>Add to my dashboard</Button>
                                        </CardActions>
                                    </Card>
                                );
                            } else if (service.serviceID === 3) {
                                return (
                                    <Card className={classes.root} key={i} variant="outlined">
                                        <CardContent>
                                            <Typography variant="h5" component="h2" style={{ textAlign: "center" }}>
                                                {service.serviceName}
                                            </Typography>
                                            <Typography color="textSecondary">
                                                <img src={service.picture} className={classes.pos} alt="image1" />
                                            </Typography>
                                            <Typography variant="body2" component="p" style={{ textAlign: "justify" }} >
                                                {service.detail}
                                                <br />
                                            </Typography>
                                            <TextField id="outlined-basic" label="Topic" style={{ width: "100%" }} onChange={changeSearchTerms} />
                                        </CardContent>
                                        <CardActions style={{ width: "100%" }}>
                                            <Button variant="contained" color="primary" size="small" className={classes.button} key={service.serviceID} id={service.serviceID} onClick={() => console.log("topic", SearchTerms) || addMeteo(service.serviceID, SearchTerms)}>Add to my dashboard</Button>
                                        </CardActions>
                                    </Card>
                                );
                            } else {
                                return (
                                    <Card className={classes.root} key={i} variant="outlined">
                                        <CardContent>
                                            <Typography variant="h5" component="h2" style={{ textAlign: "center" }}>
                                                {service.serviceName}
                                            </Typography>
                                            <Typography color="textSecondary">
                                                <img src={service.picture} className={classes.pos} alt="image2" />
                                            </Typography>
                                            <Typography variant="body2" component="p" style={{ textAlign: "justify" }} >
                                                {service.detail}
                                                <br />
                                            </Typography>
                                        </CardContent>
                                        <CardActions style={{ width: "100%" }}>
                                            <Button variant="contained" color="primary" size="small" className={classes.button} key={service.serviceID} id={service.serviceID} onClick={() => addService(service.serviceID)}>Add to my dashboard</Button>
                                        </CardActions>
                                    </Card>
                                )
                            }
                        })}
                    </div>
                </> :
                <>
                    <h3 style={{ width: "100%", textAlign: "center", marginTop: "60px", color: "green" }}> Wow, all the widgets are already added to your page! congrats!  </h3>
                    <GifPlayer gif={video} style={{ width: "400px", marginLeft: "40%", marginTop: "30px" }} />
                </>
            }
        </>
    );
}

const useStyles = makeStyles({
    rootRoot: {
        display: "flex",
        flexDirection: "row",
        flexWrap: "wrap",
        justifyContent: "space-evenly",
    },
    root: {
        maxWidth: 300,
        border: "2px solid black",
        margin: '40px'
    },
    title: {
        fontSize: 14,
        textAlign: "center",
        width: "100%"
    },
    pos: {
        display: "block",
        maxWidth: "100px",
        maxHeight: "100px",
        width: "auto",
        height: "auto",
        marginLeft: "30%",
        marginBottom: "18px",
        marginTop: "10px"
    },
    button: {
        border: "1px solid darkslategray",
        marginLeft: "18%"
    }
});