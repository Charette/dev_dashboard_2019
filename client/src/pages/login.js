import React, { useState } from 'react';
import GoogleLogin from '../components/GoogleLogin';
import { makeStyles } from "@material-ui/core";
import { Link } from "react-router-dom";
import { api } from '../config/appconfig';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import "./login.css";


const Login = ({ history: { location } }) => {
    const classes = useStyles();
    let password = "";
    let email = "";
    const [connection, setConnection] = useState([]);

    const changeEmail = (e) => {
        email = e.target.value;
        // console.log(email)
    }

    const changePass = (e) => {
        password = e.target.value;
        // console.log(email)
    }

    function connect() {
        console.log(email, password)
        // console.log(connection)
        let logged = false;
        // let history = useHistory();
        let id;
        fetch(`http://localhost:8080/logins/${email}/${password}`)
            .then(res => res.json())
            .then(
                res => {
                    setConnection(res)
                    if (res.length > 0) {
                        console.log(res[0].userID);
                        console.log("le gars existe");
                        logged = true
                        id = res[0].userID
                        window.location.href=`http://localhost:3000/user?id=${res[0].userID}#_=_`;
                    } else {
                        console.log("mauvais pass")
                    }
                }
            );
    }

    return (
        <>
            <div>
                <div className={classes.root}>
                    <h2 style={{marginTop: "20%"}}>Welcome to EPIDash!</h2>
                    <button className="loginBtn loginBtn--facebook">
                        <a href={`${api.url}/auth/facebook`}> Login with Facebook </a>
                    </button>
                    <GoogleLogin className={classes.child} ></GoogleLogin>
                    <div>
                        <TextField required id="standard-required" label="Email" onChange={changeEmail} /><br/>
                        <TextField
                            id="standard-password-input"
                            label="Password"
                            type="password"
                            autoComplete="current-password"
                            style={{marginBottom: "10px"}}
                            onChange={changePass}
                        /><br/>
                        <Button variant="outlined" color="primary" style={{margin: "0 auto", width: "100%"}} onClick={() => connect()}> Login </Button>
                    </div>
                    <Link className={classes.child} to={"/register"} >
                        <p className={classes.register}>Register</p>
                    </Link>
                </div>
            </div>
        </>
    )
}

const useStyles = makeStyles(theme => ({
    root: {
        display: "flex",
        flexDirection: 'column',
        justifyContent: "space-between",
        alignItems: "center",
        margin: "0 auto",
        height: "40vh",
        width: "55vw",
        // marginTop: "10",
    },
    child: {
        // marginTop: "10%"
    },
    register: {
        textDecoration: 'none'
    }
}));

export default Login;