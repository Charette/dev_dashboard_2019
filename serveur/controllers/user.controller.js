const User = require("../app/user.js");
const Service = require("../app/service.js");
const bcrypt = require("bcryptjs");
const sql = require("../app/db");

exports.create = (req, res) => {
  // console.log("req: ", req.body)
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }
  
  bcrypt.hash(req.body.password, 10).then((hash) => {
    const user = new User({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      password: hash
    });

    User.create(user, (err, data) => {
      // console.log(user);
      if (err)
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the Customer."
        });
      else {
        // console.log("data", data)
        res.send(data);
      }
    });
  });
};

exports.findOne = (req, res) => {
  User.findById(req.params.usersId, (err, data) => {
    // console.log("req: ", req.params);
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Customer with id ${req.params.usersId}.`
        });
      } else {
        res.status(500).send({
          message: "Error retrieving Customer with id " + req.params.usersId
        });
      }
    } else res.send(data);
  });
};

exports.findAll = (req, res) => {
  User.getAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving customers."
      });
    else res.send(data);
  });
};

exports.findService = (req, res) => {
  User.getService(req.params.usersId, (err, data) => {
    // console.log("req: ", req.params.usersId);
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found services with id ${req.params.usersId}.`
        });
      } else {
        res.status(500).send({
          message: "Error retrieving services with id " + req.params.usersId
        });
      }
    } else res.send(data);
  });
};

exports.createService = (req, res) => {
  // console.log("req: ", req.body)
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }
  
  const service = new Service({
    userID: req.body.userID,
    serviceID: req.body.serviceID,
    setting1: req.body.setting1,
    setting2: req.body.setting2,
    setting3: req.body.setting3,
    setting4: req.body.setting4,
    setting5: req.body.setting5,
  });

  Service.createService(service, (err, data) => {
    // console.log(user);
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Customer."
      });
    else res.send(data);
  });
};

exports.login = (req, res) => {

    User.log(req.params.usersId, req.params.password, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `bad password for ${req.params.email}.`
          });
        } else {
          res.status(500).send({
            message: "bad password for " + req.params.email
          });
        }
      } else res.send(data);
    });
};

exports.findAllService = (req, res) => {
  Service.getAll(req.params, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving services."
      });
    else res.send(data);
  });
};

exports.delete = (req, res) => {
  Service.remove(req.params, (err, data) => {
    // console.log(req.params)
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Customer with id ${req.params.customerId}.`
        });
      } else {
        res.status(500).send({
          message: "Could not delete Customer with id " + req.params.customerId
        });
      }
    } else res.send({ message: `Customer was deleted successfully!` });
  });
};