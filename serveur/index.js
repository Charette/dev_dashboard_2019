var express = require('express');
const passport = require('passport');
const FacebookStrategy = require('passport-facebook').Strategy;
const session = require('express-session')
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const config = require('./config/db.config.js');
var fs = require('fs');
const hostname = '127.0.0.1';
const port = 8080;
var app = express();
const pool = require("./app/db.js");
var request = require('request'); // "Request" library
var querystring = require('querystring');
const sql = require("../serveur/app/db");


const NewsAPI = require('newsapi');
const newsapi = new NewsAPI('c9319a917d3d4eac97abc7bcaf33701d');

// newsapi.v2.topHeadlines({
//   sources: 'bbc-news,the-verge',
//   q: 'bitcoin',
//   category: 'business',
//   language: 'en',
//   country: 'us'
// }).then(response => {
//   console.log(response);
// });

newsapi.v2.everything({
  q: 'bitcoin',
  domains: 'engadget.com, techcrunch.com, washingtonpost.com',
  from: '2020-04-22',
  to: '2017-04-22',
  language: 'en',
  sortBy: 'relevancy',
  page: 2
}).then(response => {
  // console.log(response);
});

// newsapi.v2.sources({
//   category: 'technology',
//   language: 'en',
//   country: 'us'
// }).then(response => {
//   console.log(response);
// });


var client_id = '18a1abd8a21545fd8c56db1d20f25034'; // Your client id
var client_secret = '9c380c2802d74a058664cbc62f12f5de'; // Your secret
var redirect_uri = 'http://localhost:8080/callback'; // Or Your redirect uri

/**
 * Generates a random string containing numbers and letters
 * @param  {number} length The length of the string
 * @return {string} The generated string
 */
var generateRandomString = function (length) {
  var text = '';
  var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

  for (var i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
};

var user = "";

var stateKey = 'spotify_auth_state';

var app = express();

app.use(express.static(__dirname + '/public'))
  .use(cookieParser());

app.get('/loginS', function (req, res) {

  var state = generateRandomString(16);
  res.cookie(stateKey, state);

  // your application requests authorization
  var scope = 'user-read-private user-read-email user-read-playback-state';
  res.redirect('https://accounts.spotify.com/authorize?' +
    querystring.stringify({
      response_type: 'code',
      client_id: client_id,
      scope: scope,
      redirect_uri: redirect_uri,
      state: state
    }));
});

app.get('/callback', function (req, res) {

  // your application requests refresh and access tokens
  // after checking the state parameter

  var code = req.query.code || null;
  var state = req.query.state || null;
  var storedState = req.cookies ? req.cookies[stateKey] : null;

  if (state === null || state !== storedState) {
    res.redirect('/#' +
      querystring.stringify({
        error: 'state_mismatch'
      }));
  } else {
    res.clearCookie(stateKey);
    var authOptions = {
      url: 'https://accounts.spotify.com/api/token',
      form: {
        code: code,
        redirect_uri: redirect_uri,
        grant_type: 'authorization_code'
      },
      headers: {
        'Authorization': 'Basic ' + (new Buffer(client_id + ':' + client_secret).toString('base64'))
      },
      json: true
    };

    request.post(authOptions, function (error, response, body) {
      if (!error && response.statusCode === 200) {

        var access_token = body.access_token,
          refresh_token = body.refresh_token;

        var options = {
          url: 'https://api.spotify.com/v1/me',
          headers: { 'Authorization': 'Bearer ' + access_token },
          json: true
        };

        // use the access token to access the Spotify Web API
        request.get(options, function (error, response, body) {
          console.log(body);
        });

        // we can also pass the token to the browser to make requests from there
        console.log("backuser", user)
        res.redirect(`http://localhost:3000/dashboard?id=${user}/#` +
          querystring.stringify({
            access_token: access_token,
            refresh_token: refresh_token
          }));
      } else {
        res.redirect('/#' +
          querystring.stringify({
            error: 'invalid_token'
          }));
      }
    });
  }
});

app.get('/refresh_token', function (req, res) {

  // requesting access token from refresh token
  var refresh_token = req.query.refresh_token;
  var authOptions = {
    url: 'https://accounts.spotify.com/api/token',
    headers: { 'Authorization': 'Basic ' + (new Buffer(client_id + ':' + client_secret).toString('base64')) },
    form: {
      grant_type: 'refresh_token',
      refresh_token: refresh_token
    },
    json: true
  };

  request.post(authOptions, function (error, response, body) {
    if (!error && response.statusCode === 200) {
      var access_token = body.access_token;
      res.send({
        'access_token': access_token
      });
    }
  });
});


// Passport session setup.
passport.serializeUser(function (user, done) {
  done(null, user);
});

passport.deserializeUser(function (obj, done) {
  done(null, obj);
});

passport.use(new FacebookStrategy({
  clientID: config.facebook_api_key,
  clientSecret: config.facebook_api_secret,
  callbackURL: config.callback_url,
  // passReqToCallback : true,
  profileFields: ['id', 'email', 'first_name', 'last_name', 'picture.type(small)']
},
  function (accessToken, refreshToken, profile, done) {
    console.log(profile)
    process.nextTick(function () {
      if (config.use_database) {
        user = profile.id
        pool.query("SELECT * from user where userID=" + parseInt(profile.id) + ";", (err, rows) => {
          if (err) throw err;
          if (rows && rows.length === 0) {
            console.log("There is no such user, adding now");
            pool.query("INSERT into user (userID, firstName, lastName, email, picture) VALUES('" + profile.id + "','" + profile.name.givenName + "', '" + profile.name.familyName + "', '" + profile.emails[0].value + "', '" + profile.photos[0].value + "')");
          } else {
            console.log("User already exists in database");
          }
        });
      }
      return done(null, profile);
    });
  }
));

// app.set('views', __dirname + '/views');
// app.set('view engine', 'ejs');
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({ secret: 'keyboard cat', key: 'sid', resave: true, saveUninitialized: true, cookie: { secure: false } }));
app.use(passport.initialize());
app.use(passport.session());
//app.use(express.static(__dirname + '/public'));

app.get('/', function (req, res) {
  res.render('/auth/facebook');
});

app.get('/user', function (req, res) {
  res.json({ user: req.user._json });
});

// premier lien de connexion
app.get('/auth/facebook', passport.authenticate('facebook', { scope: 'email' }));

//lien de redirection apres login (fail ou reussi)
app.get('/auth/facebook/callback', passport.authenticate('facebook', { failureRedirect: '/auth/facebook' }), (req, res) => {
  res.redirect('http://localhost:3000/user?id=' + req.user.id); //a completer par root front
});

// app.get('/logins/:email/:pass',
//   passport.authenticate('local'),
//   function(req, res) {
//     sql.query(`SELECT * FROM user WHERE email = '${req.params.email}' and password = '${req.params.pass}'`, (err, res) => {
//       if (err) {
//           console.log("error: ", err);
//           result(null, err);
//           return;
//       }
//     })
//     res.redirect('/' + req.user.username);
  // });

//deconnexion
app.get('/logout', function (req, res) {

  req.logout();
  req.session.destroy(function (err) {
    res.redirect('http://localhost:3000/'); //Inside a callback… bulletproof!
  });

  // req.logout();
  // res.clearCookie('sid');
  // res.redirect('http://localhost:3000/');
});

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) { return next(); }
  res.redirect('/login')
}

app.get('/about.json', function (req, res) {
  var file = fs.readFileSync('./about.json', 'utf8');
  res.send(file);
})

require("./app/routes")(app);
app.listen(port, function () {
  // console.log(`Example app listening on port ${hostname}:${port} !`)
})