const express = require("express"),
router = new express.Router(),
passport = require('passport'),
request1 = require('axios');

const getFriendsList = (id) => {
  return request1.get(`http://api.steampowered.com/ISteamUser/GetFriendList/v0001/?key=A6B2E877BA720060C2C4ECD951A00179&steamid=${id}&relationship=friend`);
}
const getProfile = (id) => {
  return request1.get(`http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=A6B2E877BA720060C2C4ECD951A00179&steamids=${id}`);
}

router.get('/profile/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
  getProfile(req.params.id).then((resSteam) => {
    res.status(200).json({
      success: true,
      message: 'Steam profile',
      profiles: resSteam.data.response.players
    });
  }).catch((err) => {
    res.status(500).json({
      success: false,
      message: 'Error while getting steam response',
      error: err
    })
  })
})
router.get('/friends/:id', passport.authenticate('jwt', { session: false }), (req, res) => {
  getFriendsList(req.params.id).then((resSteam) => {
    res.status(200).json({
      success: true,
      message: 'Steam friends list',
      friends: resSteam.data.friendslist.friends
    })
  }).catch((err) => {
    res.status(500).json({
      success: false,
      message: 'Error while getting steam response',
      error: err
    })
  })
})
router.get('/profile', passport.authenticate('jwt', { session: false}), (req, res) => {
  const steamWidget = req.user.widgets.find((widget) => widget.widgetKey === "steamProfile");
  if (steamWidget) {
    getProfile(steamWidget.params.steamID).then((resSteam) => {
      res.status(200).json({
        success: true,
        message: 'Steam profile',
        profiles: resSteam.data.response.players
      });
    }).catch((err) => {
      res.status(500).json({
        success: false,
        message: 'Error while getting steam response',
        error: err
      })
    })
  } else
    res.status(404).json({
      success: false,
      message: 'Steam widget not found'
    })
})
router.get('/friends', passport.authenticate('jwt', { session: false }), (req, res) => {
  const steamWidget = req.user.widgets.find((widget) => widget.widgetKey === 'steamFriends');
  if (steamWidget) {
    getFriendsList(steamWidget.params.steamID).then((resSteam) => {
      res.status(200).json({
        success: true,
        message: 'Steam friends list',
        profiles: resSteam.data.friendslist.friends
      });
    }).catch((err) => {
      res.status(500).json({
        success: false,
        message: 'Error while getting steam response',
        error: err
      })
    })
  } else
    res.status(404).json({
      success: false,
      message: 'Steam widget not found'
    })
})
module.exports = router;