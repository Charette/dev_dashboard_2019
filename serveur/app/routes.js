module.exports = app => {
    const users = require("../controllers/user.controller.js");

    app.post("/users", users.create);

    app.post("/services", users.createService);

    app.get("/logins/:usersId/:password", users.login);

    app.get("/users", users.findAll);

    app.get("/users/:usersId", users.findOne);

    app.get("/services/:usersId", users.findService);

    app.get("/notServices/:userId", users.findAllService);

    app.delete("/services/:usersId/:serviceId", users.delete);
};