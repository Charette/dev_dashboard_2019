const sql = require("./db");

const Service = function (service) {
    this.userID = service.userID;
    this.serviceId = service.serviceID;
    this.setting1 = service.setting1;
    this.setting2 = service.setting2;
    this.setting3 = service.setting3;
    this.setting4 = service.setting4;
    this.setting5 = service.setting5;
}

Service.createService = (newService, result) => {
    console.log("newUser :", newService)
    sql.query("INSERT INTO service_user SET ?", newService, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        console.log("created User: ", { id: res.insertId, ...newService });
        result(null, { id: res.insertId, ...newService });
    });
};

Service.getAll = (id, result) => {
    // console.log("id: ", id);
    sql.query("select * from dashboard.service where dashboard.service.serviceID NOT IN (select service.serviceID from dashboard.service, dashboard.service_user where dashboard.service.serviceID = dashboard.service_user.serviceID and dashboard.service_user.userID = ?);", id.userId, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        // console.log("users: ", res);
        result(null, res);
    });
};

Service.remove = (id, result) => {
    console.log(id)
    sql.query(`DELETE FROM service_user where userID = ${id.usersId} and serviceID = ${id.serviceId}`, id, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      if (res.affectedRows == 0) {
        // not found Customer with the id
        result({ kind: "not_found" }, null);
        return;
      }
  
      console.log("deleted customer with id: ", id);
      result(null, res);
    });
  };

module.exports = Service;