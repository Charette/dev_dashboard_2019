const sql = require("./db");
var bcrypt = require('bcryptjs');


const User = function (user) {
  this.firstName = user.firstName;
  this.lastName = user.lastName;
  this.email = user.email;
  this.password = user.password;
};

User.create = (newUser, result) => {
  console.log("newUser :", newUser.password)
  
  sql.query("INSERT INTO user SET ?", newUser, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);  
      return;
    }

    console.log("created User: ", { id: res.insertId, ...newUser });
    result(null, { id: res.insertId, ...newUser });
  });
};

User.findById = (userId, result) => {
  sql.query(`SELECT * FROM user WHERE userID = ${userId}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found user: ", res[0]);
      result(null, res[0]);
      return;
    }

    // not found Customer with the id
    result({ kind: "not_found" }, null);
  });
};

User.getAll = result => {
  sql.query("SELECT * FROM user", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("users: ", res);
    result(null, res);
  });
};

User.getService = (userId, result) => {
  // console.log("userid: ", userId);
  sql.query(`SELECT distinct service.serviceID, service.serviceName, service_user.setting1, service_user.setting2, service_user.setting3, service_user.setting4, service_user.setting5 from service, service_user, user where service.serviceID = service_user.serviceID and service_user.userID = ${userId}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }
    // console.log("service: ", res);
    result(null, res);
  });
};

User.log = (email, password, result) => {
  // console.log("email: ", email, "password: ", password)
    // sql.query(`SELECT * FROM user WHERE email = '${email}' and password='${password}'`, (err, res) => {
    sql.query(`SELECT * FROM user WHERE email = '${email}'`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      // console.log(res)


      // let test = JSON.stringify(res)
      // console.log(password, res[0])
      bcrypt.compare(password, res[0].password, function(err, test) {
        console.log(res);
        if (test === true) {
          result(null, res)
        } else {
          console.log("bad password")
        }
      })
      // console.log(res[0].password)

      // result(null, res);
    });
};

module.exports = User;