CREATE DATABASE dashboard;

USE dashboard;

CREATE TABLE user (
	userID BIGINT NOT NULL PRIMARY KEY auto_increment,
	firstName varchar(50) NOT NULL,
	lastName varchar(50) NOT NULL,
	email varchar(100) NOT NULL,
	password varchar(100)
);